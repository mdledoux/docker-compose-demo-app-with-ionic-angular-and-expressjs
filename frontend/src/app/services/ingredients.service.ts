import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
const { restURL } = environment.ingredients;

export interface Ingredient {
  id: number,
  item: string
};


@Injectable({
  providedIn: 'root'
})
export class IngredientsService {
  public items$: Observable<Ingredient[]> = interval(5000).pipe(
    switchMap(  (i: number) => this.http.get<Ingredient[]>(restURL)  )
  );

  constructor(private http: HttpClient) { }

}
