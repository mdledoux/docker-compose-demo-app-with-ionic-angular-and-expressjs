import { Component, OnInit } from '@angular/core';
import { Ingredient, IngredientsService } from '../services/ingredients.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  private ingredients$ = this.ingredientsSvc.items$;

  constructor(
    private ingredientsSvc: IngredientsService
  ) {}

  ngOnInit() {
    this.ingredients$.subscribe((ing: Ingredient[]) => console.log("ingredients: ", ing) );
  }

}
