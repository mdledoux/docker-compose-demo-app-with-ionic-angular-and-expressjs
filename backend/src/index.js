console.log("Hello World - port:", process.env.BACKEND_PORT_INT)
//https://www.section.io/engineering-education/how-to-use-cors-in-nodejs-with-express/
const express = require('express');
const app = express();
const cors = require('cors');
const shuffle = require('./shuffle.js');


const ingredientsOrig = [
  {
    "id": "1",
    "item": "Bacon"
  },
  {
    "id": "2",
    "item": "Eggs"
  },
  {
    "id": "3",
    "item": "Milk"
  },
  {
    "id": "4",
    "item": "Butter"
  }
];
const ingredients = shuffle(ingredientsOrig);

app.use(cors({
  origin: [
    'http://10.0.0.202:8000',  // ng webpack-dev-server
    'http://10.0.0.202:8080',  // nginix
    'http://cems.dev.local:8000',
    'http://cems.dev.local:8080',
    'http://myapp.dev.local:8000',
    'http://myapp.dev.local:8080',
    `http://${process.env.GATEWAY_HOST}:${process.env.GATEWAY_PORT}`
  ]
}));


app.get('/ingredients', (req, res) => {
  res.send(ingredients);
});
app.listen(process.env.BACKEND_PORT_INT);
