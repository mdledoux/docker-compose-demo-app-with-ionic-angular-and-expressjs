FROM postgres:12-alpine AS psql
COPY ./db/init/ /docker-entrypoint-initdb.d/
EXPOSE 5432




# docker run -it --rm -v $PWD:/app nodejs bash
FROM node:14 AS nodejs
RUN npm i -g npm@8
RUN npm i -g @angular/cli@12.1 @ionic/cli
RUN echo "alias ls='ls --color'" >> ~/.bashrc
ARG UID=0
USER $UID
WORKDIR /app


# ionic start frontend --type=angular --capacitor
FROM nodejs AS ng_frontend
WORKDIR /frontend
CMD npm install   &&   ng serve --host 0.0.0.0 --disable-host-check


FROM ng_frontend AS ng_frontend_install_build
COPY ./frontend/*.json ./frontend/*.js ./
COPY ./frontend/src/ ./src/
RUN npm install
ARG CONFIGURATION=production
RUN ng build
              # --configuration=production
              # --configuration=$CONFIGURATION



FROM nodejs as backend
WORKDIR /backend
CMD npm install   &&   npm start

FROM backend AS backend_install
COPY ./backend/package.json ./backend/package-lock.json  ./
COPY ./backend/src/ ./src/
RUN npm install
CMD npm run start:staging



FROM nginx:1 as gateway
COPY --from=ng_frontend_install_build /frontend/www  /usr/share/nginx/html