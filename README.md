# MyApp
This is the culimination of nearly 3 years of experience with Docker.

This demo takes a bunch of best-practices, and individual concepts of Docker (with a simple NodeJS app and Angular/Ionic, along with a DB)

## Where to find this?
https://gitlab.com/mdledoux/docker-compose-demo-app-with-ionic-angular-and-expressjs/

### Why GitLab, rather than GitHub?
Good question!  There isn't a great reason except that:
1. You are all already familiar with the GitLab UI
1. GitLab is a bit more valuable for showing a CI demo consistent with UNH's environment:

## Order of Demo

- Start a blank project:
  - touch .env
  - touch Dockerfile
  - touch docker-compose.yml
  - mkdir -p {frontend,backend,db/init}
  - touch frontend/FRAME_WORK_HERE backend/EXPRESS_HERE
  - touch db/init/000-ingredients-create.sql
  - touch db/init/000-ingredients-data.sql
- Make a basic PostGreSQL:12-alpine **image** target:  `psql` \
    https://hub.docker.com/_/postgres
  - `docker run -it --rm   postgres bash`
  - `docker run -it --rm   postgres su postgres`
  - `EXPOSE 5432` for internal access
  - **populate SQL files, then add**:
  - `COPY ./db/init/ /docker-entrypoint-initdb.d/`
  - `docker build -t my_psql .   --target psql`
  - docker-run it again (a couple times - show it re-initializing each time)
    - next we will add a volume, but later - we won't do it at the CLI but with `docker-compose`
- Add a `node:12` images target: `nodejs`
  - install npm 8
  - WORKDIR /app
  - for fun, install angular@12.1 and ionic cli
  - type out `docker build` command 
    - write out command into `build.sh`: \
      `docker build -t nodejs . --target  nodejs`
    - run container:  volume mount $PWD:/app \
      `docker run -it --rm -v $PWD:/app nodejs bash`
    - do a demo of "ionic start" or even just writing a file
      - **demonstrate user privs:** \
        touch a file, and exit.  Delete it now.
    - first fix color - after npm@8, add: \
      `RUN echo "alias ls='ls --color'" >> ~/.bashrc`
    - fix privs:
      - echo $UID
      - add a build-time ARG, and set USER to that UID:
        ```Dockerfile
        ARG UID=0
        USER $UID
        ```
      - add `--build-arg` to build command: \
        `docker build -t nodejs . --target  nodejs  --build-arg UID=1000`
      - Re-BUILD: \
        `./build.sh`
      - Re-RUN: \
        `docker run -it --rm -v $PWD:/app nodejs bash`
        - touch a file, and exit.  Delete it now.
- Add a `ng_frontend` target from `nodejs`
  ```Dockerfile
  WORKDIR /frontend
  CMD echo "FRONTEND" && tail
  ``` 
- Add a `backend` target from `nodejs`
  ```Dockerfile
  WORKDIR /backend
  CMD echo "BACKEND" && tail
  ```
- Create the backend module:
  ```bash
  npm init  
  # ....
  npm install -D esm nodemon
  ```
- backend/src/index.js: \
  `console.log("Hello World from BACKEND");`
- package.json scripts:
  ```json
    "scripts": {
      "start": "node -r esm src/index.js",
  ```
- Run from nodejs container
- `docker-compose.yml` time!
  - start with a .env:
    ```
    DB_USER=postgres
    DB_PASS=1234
    DB_NAME=myapp
    DB_HOST=db
    DB_PORT=5432
    POSTGRES_HOST=localhost

    UID=1000

    FRONTEND_PORT_EXT=8000
    FRONTEND_PORT_INT=4200
    BACKEND_PORT_INT=5000
    BACKEND_PORT_EXT=5000
    ```
  - `docker-compose.yml`:
    ```yml
    version: '3.7'
    services:
      db:
        image: my_psql
        build:
          target: psql
          context: .
        environment:
          POSTGRES_USER: $DB_USER
          POSTGRES_PASSWORD: $DB_PASS
          POSTGRES_DB: $DB_NAME
          POSTGRES_HOST: $POSTGRES_HOST
          POSTGRES_PORT: $DB_PORT


      frontend:
        image: my_ng_frontend
        build:
          target: ng_frontend
          context: .
          args:
            UID: ${UID}
        user: $UID
        ports:
        - ${FRONTEND_PORT_EXT}:${FRONTEND_PORT_INT}
        volumes:
          - ./frontend:/frontend


      backend:
        image: my_express_backend_dev
        build:
          target: backend
          context: .
          args:
            UII: ${UID}
        volumes:
          - ./backend:/backend
        user: $UID
        environment:
          BACKEND_PORT_INT: ${BACKEND_PORT_INT}
        ports:
          - ${BACKEND_PORT_EXT}:${BACKEND_PORT_INT}
    ```
- `docker-compose up --build`
  - `docker ps`: 3 containers shuold be running.
  - Try the DB:
    `docker exec -it myapp_db_1 su postgres`
    ```
    \l
    \dt
    ```
    The data should be there!

- Add persistant storage to DB:
  ```yml
  db:
    volumes:
      - type: volume
        source: pgdata
        target: /var/lib/postgresql/data
      - type: bind
        source: ./db/init/
        target: /docker-entrypoint-initdb.d/
      #- ./db/init/:/docker-entrypoint-initdb.d/  

  volumes:
    pgdata:
      driver: local
  ```
  - restart docker-compose, show that db does not initialize after the first time. \
  !

- Now create `backend`:
  - change backend `CMD` from tail to:
    ```bash
    npm install  &&  npm start
    ```
  - restart docker-compose
  - container should start and stop immediatel
  - alter `"start"` script: \
    `nodemon --watch src -r esm src/index.js`
  - restart docker-compose
  - backend container should stay running, thanks to node mon
    - demonstrate an edit to `backend/src/index.js`
    - the container restarts
- Finally, augment `package.json`:
  ```json
  "scripts": {
    "start": "npm run start:dev",
    "start:dev": "nodemon --watch src -r esm src/index.js",
    "start:staging": "node -r esm src/index.js",
  ```
  - create an ExpresJS app: out of index.js: \
    https://www.section.io/engineering-education/how-to-use-cors-in-nodejs-with-express/
    - substitutue `6069` for `process.env.BACKEND_PORT_INT`
    - do CORS later.
- Let's make the frontend app!
  - spin up a plain-old NodeJS container from the image we built (which contains npm 8, Angular CLI, Ionic CLI) \
  `docker run -it --rm -v $PWD:/app nodejs bash`
  - Create an ionic project \
    `ionic start frontend tabs --type=angular --capacitor`
    - YES, overwrite `frontend`
  - set the CMD in Dockerfile:
    ```Dockerfile
    WORKDIR /frontend
    CMD npm install   &&   ng serve --host 0.0.0.0 --disable-host-check
    # CMD echo "FRONTEND" && tail
    ``` 
  - restart docker-compose
    - packages will install, Angular will compile and run.
  - Navigate to http://cems.dev.local:8000 / myapp.dev.local
  - Navigate to http://cems.dev.local:5000 / myapp.dev.local
  - Just SHOW CORS issue: \
    `fetch("http://cems.dev.local:5000/ingredients").then(req => req.text()).then(console.log)`
    - Maybe fix CORS issue now (or later):
      ```js
      app.use(cors({
        origin: [
          'http://10.0.0.202:8000',  // ng webpack-dev-server
          'http://10.0.0.202:8080',  // nginix
          'http://myapp.dev.local:8000',
          'http://myapp.dev.local:8080',
        ]
      }));

      app.get('/ingredients', (req, res) => {
      ```
    - maybe add:
      ```js
      `http://${process.env.GATEWAY_HOST}:${process.env.GATEWAY_PORT}`,
      ```
    - fetch again: \
    `fetch("http://cems.dev.local:5000/ingredients").then(req => req.text()).then(console.log)`

  - Angular pieces:
    - Add `IngredientsService`: \
      `ng generate service services/ingredients`
    - app.module.ts:
      import: `HttpClientModule`
      - `import { HttpClientModule } from '@angular/common/http';`
      - `imports: [ HttpClientModule, ],`
    - ingredient.service.ts:
      - inject HttpClient: \
        `constructor(private http: HttpClient) { }`
      - Add an interface:
        ```ts
        export interface Ingredient {
          id: number,
          item: string
        };
        ```
      - Make the data source: \
        `public items$: Observable<Ingredient[]> = this.http.get<Ingredient[]>(restURL);`
    - Use the `app/tab2` for demo:
      - `import { Ingredient, IngredientsService } from '../services/ingredients.service';
      - `  constructor( private ingredientsSvc: IngredientsService ) {}`
      - `class Tab2Page implements OnInit`
      - Demo that it works - subscribe: \
        `ngOnInit() { this.ingredients$.subscribe((ing: Ingredient[]) => console.log("ingredients: ", ing) ); }`
      - html template:
        ```html
        <ion-content [fullscreen]="true">
          <ion-list *ngFor="let ing of ingredients$ | async">
            <ion-item>
              <ion-label>{{ ing.item }}</ion-label>
            </ion-item>
          </ion-list>
        </ion-content>
        ```
      - comment out the subscribe()
    - Voila!  You have a working app
- prepare to set up staging:
  - Add images with built copies of code (non dev-mode) inside them.  Angular is compiled to static files - we can not rely on our locally mounted volume in a staging or production environment - the source-code must be baked into the image to spawn containers from:
    - Add a installed version of backend, `backend_install`
      ```Dockerfile
      FROM backend AS backend_install
      COPY ./backend/package.json ./backend/package-lock.json  ./
      COPY ./backend/src/ ./src/
      RUN npm install
      CMD npm run start:staging
      ```

    - Add `ng_frontend_install_build`:
      ```Dockerfile
      FROM ng_frontend AS ng_frontend_install_build
      COPY ./frontend/*.json ./frontend/*.js ./
      COPY ./frontend/src/ ./src/
      RUN npm install
      ARG CONFIGURATION=production
      RUN ng build
                    # --configuration=production
                    # --configuration=$CONFIGURATION
      ```
    - Finally, add an nginx server as a gateway (or a static HTML/JS host, as the case may be)
      ```Dockerfile
      FROM nginx:1 as gateway
      COPY --from=ng_frontend_install_build /frontend/www  /usr/share/nginx/html      
      ```
  - extract stuff out of `docker-compose.yml` into `docker-compose.devel.yml`:
    - `frontend` comes out of entirely:
      ```yml
        frontend:
          image: my_ng_frontend
          build:
            target: ng_frontend
            context: .
            args:
              UID: ${UID}
          user: $UID
          ports:
          - ${FRONTEND_PORT_EXT}:${FRONTEND_PORT_INT}

          volumes:
            - ./frontend:/frontend
      ```
    - backend (build and volumes):
      ```yml
        backend:
          image: my_express_backend_dev
          build:
            target: backend
            context: .
            args:
              UII: ${UID}
          volumes:
            - ./backend:/backend
      ```
    - db (just volumes)
      ```yml
        db:
          volumes:
            - type: volume
              source: pgdata
              target: /var/lib/postgresql/data
            - type: bind
              source: ./db/init/
              target: /docker-entrypoint-initdb.d/
            #- ./db/init/:/docker-entrypoint-initdb.d/
      ```
      and add to bottom:
      ```yml
      volumes:
        pgdata:
          driver: local
      ```
  - finally, make a companion `docker-compose.staging.yml` - this will include the nginx gateway, but NOT the frontend angular dev server:
    ```yml
    version: '3.7'
    services:
      db:
        volumes:
          - type: volume
            source: pgdata
            target: /var/lib/postgresql/data
          # - type: bind
          #   source: ./db/init/
          #   target: /docker-entrypoint-initdb.d/
          #- ./db/init/:/docker-entrypoint-initdb.d/



      backend:
        image: my_express_backend_staging
        build:
          target: backend_install
          context: .
          args:
            UII: ${UID}


      gateway:
        image: my_gateway_staging
        #IGNORE IN SWARM:
        build:
          target: gateway
          context: .
          args:
            CONFIGURATION: production
        ports: 
          # - 8080:80
          - ${GATEWAY_PORT}:80
        environment:
          GATEWAY_PORT: ${GATEWAY_PORT}
          GATEWAY_HOST: ${GATEWAY_HOST}
        #   NGINX_HOST: ${GATEWAY_HOST}
        #   NGINX_PORT: ${GATEWAY_PORT}



    volumes:
      pgdata:
        driver: local

    ```
- Optionally, add a .gitlab-ci.yml







  


  





