#!/bin/bash

#CONTAINER IMAGES:
docker build -t my_psql                     . --target backend_install
docker build -t my_express_backend_staging  . --target backend_install
docker build -t my_gateway_staging          . --target gateway


docker image tag  my_psql:latest                    hub.unh.edu:5000/docker/c3ms/my_psql:latest
docker image tag  my_express_backend_staging:latest hub.unh.edu:5000/docker/c3ms/my_express_backend_staging:latest
docker image tag  my_gateway_staging:latest         hub.unh.edu:5000/docker/c3ms/my_gateway_staging:latest


docker image push hub.unh.edu:5000/docker/c3ms/my_psql:latest
docker image push hub.unh.edu:5000/docker/c3ms/my_express_backend_staging:latest
docker image push hub.unh.edu:5000/docker/c3ms/my_gateway_staging:latest



#NON-CONTAINER IMAGES:
docker build -t nodejs . --target  nodejs  --build-arg UID=1000





    # - docker build . -t maintenance_tasks:latest --target node_maintenance_tasks
    # - docker image tag  maintenance_tasks:latest   hub.unh.edu:5000/docker/c3ms/maintenance_tasks:latest
    # - docker image push hub.unh.edu:5000/docker/c3ms/maintenance_tasks:latest